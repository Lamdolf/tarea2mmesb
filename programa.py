# /usr/bin/python3
import sys
import time
import os
import estimators
from statistics import mean, stdev
from prettytable import PrettyTable, ALL

PATTERNS = ["CTA", "CTT", "CTG", "CTC", "TAG", "AAG", "CAG", "GAG"]

# Transform raw input into useful data
def preprocessData(rawData):

    cutPoints = []

    # Getting how many def lines are 
    # (more than 1 means more than 1 genomic region)
    for line in range(len(rawData)):
        if rawData[line][0] == ">":
            cutPoints.append(line)

    # Remove all deflines and line breaks
    if len(cutPoints) == 1:
        processedData = rawData[1:]
        processedData = "".join(list(map(str.strip, processedData)))

    # Do the same in case of multiple regions, and separate them by an empty
    # space
    else:
        processedData = []
        
        for cut in range(1, len(cutPoints)):
            processedData.append(rawData[cutPoints[cut-1]+1:cutPoints[cut]])
            
        processedData.append(rawData[cutPoints[len(cutPoints)-1]+1:])

        for section in range(len(processedData)):
            processedData[section] = "".join(list(map(str.strip,
                                                      processedData[section])))

        processedData = " ".join(processedData)

    return processedData


# Read file from system
def getData(FILE):

    try:
        with open(FILE) as file:
            data = file.readlines()
            file.close()

    except IOError:
        printf("El archivo especificado no existe o la ruta no es correcta")
        exit(1)

    return data


# Count the real amount of ocurrences for the given patterns
def countPatterns(data):

    observed = {"CTA": 0, "CTT": 0, "CTG": 0, "CTC": 0,
                "TAG": 0, "AAG": 0, "CAG": 0, "GAG": 0}

    for codon in range(len(data)-2):
        if data[codon]+data[codon+1]+data[codon+2] in PATTERNS:
            observed[data[codon]+data[codon+1]+data[codon+2]] += 1

    return observed

def outputTable(outputData, observed, GC):

    table = PrettyTable()
    table.header = False
    table.hrules = ALL
    table.add_row(["Pattern", "CTA", "CTT", "CTG", "CTC",
                   "CTA", "CTT", "CTG", "CTC", "Promedio", "Desviación Estándar"])
    table.add_row(["Strain", "(+)", "(+)", "(+)", "(+)", "(-)", "(-)", "(-)", "(-)", "N/A", "N/A"])
    table.add_row(["%%G+C"]+[GC for i in range(8)] + ["N/A", "N/A"])
    table.add_row(["N° Observaciones"] + [x for x in observed.values()] + ["N/A", "N/A"])

    for i in range(6):
        estimation = [x for x in outputData[i]["Prediction"].values()]
        errors = [(abs(observed[x] - outputData[i]["Prediction"][x]) / observed[x]) for x in observed]
        times = [outputData[i]["Time"] for x in range(8)]
        table.add_row(["Estimación " + str(i+1)] + estimation + [mean(estimation), stdev(estimation)])
        table.add_row(["Error relativo " + str(i+1)] + errors + [mean(errors), stdev(errors)])
        table.add_row(["Tiempo de cálculo " + str(i+1) + " (s)"] + times + [mean(times), stdev(times)])
    print(table)

def GCPercentage(data):

    breaks = data.count(" ")
    GC = 0
    for base in data:
        if base in "CG":
            GC += 1

    return (GC / (len(data) - breaks))

def main(FILE):

    raw = getData(FILE)
    data = preprocessData(raw)
    realCount = countPatterns(data)
    GC = GCPercentage(data)

    output = []
    output.append(estimators.noInfoMethod(data))
    output.append(estimators.compositionInfoMethod(data))
    output.append(estimators.markovMethod(data))
    output.append(estimators.markovMethod(data, invert=True))
    output.append(estimators.allBaseDependencyMethod(data))
    output.append(estimators.allBaseDependencyMethod(data, invert=True))

    outputTable(output, realCount, GC)


if __name__ == "__main__":

    # Check for a valid input 
    if len(sys.argv) == 2:
        FILE = sys.argv[1]
        main(FILE)

    else:
        print("Uso erróneo del programa")
        print("Correcto modo de uso: python3 programa.py <archivo.fna>")
        exit(1)
