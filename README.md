# Tarea 2 MMESB

- Creado por: Arturo Lobos Castillo y Benjamín Rojas Rojas.
- Docente: Gonzalo Riadi Mahias.
- Ayudantes: Camilo Fuentes Beals y Daniela Araya Ortega.
- Módulo: Modelos Matemáticos en Sistemas Biológicos.


## Organismos utilizados

- Mycoplasma Genitalium G37 (https://www.ncbi.nlm.nih.gov/genome/474?genome_assembly_id=300158)
- Streptomyces coelicolor str. M1154/pAMX4/pGO1416 (https://www.ncbi.nlm.nih.gov/genome/1057?genome_assembly_id=909299)
- Yersinia pestis str. M2085 (https://www.ncbi.nlm.nih.gov/genome/153?genome_assembly_id=1488158)
- Pseudomonas fluorescens str. ATCC 13525 (https://www.ncbi.nlm.nih.gov/genome/150?genome_assembly_id=329889)
- Escherichia Coli str. K-12 (https://www.ncbi.nlm.nih.gov/genome/167?genome_assembly_id=161521)

## Patrones a buscar

- CTA
- CTT
- CTG
- CTC

Estos también se han buscado en las hebras complementarias.

## Construcción del programa

- Sistema operativo: Arch Linux.
- NeoVim 0.5.0: Editor de texto utilizado para escribir el código del programa.
- Python 3.9.5: Lenguaje utilizado para resolver la problemática.

## Dependencias

- Python en version >= 3.6
- [PrettyTable](https://github.com/jazzband/prettytable)
## Puntos a considerar

- Se debe realizar la instalación de la librería "PrettyTable" para que no ocurra
ningún problema en la ejecución, esto con:
```
python -m pip install -U prettytable
python -m pip install -U git+https://github.com/jazzband/prettytable
```
para obtener la última versión de la librería.
- En caso de no contar con pip, se debe instalar acorde a las instrucciones detalladas
en el siguiente link: https://pip.pypa.io/en/stable/installing/.
- La ejecución del programa se lleva a cabo mediante el comando "python3", seguido
de programa.py y del archivo en el cual se encuentra el genoma; quedando del siguiente
modo:
```
pyhton3 programa.py genoma.fna
```
